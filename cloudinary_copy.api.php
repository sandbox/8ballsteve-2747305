<?php

/**
 * @file
 * Hooks provided by cloudinary_copy.
 */

/**
 * Allow modules to flag a file as excluded from copy.
 *
 * Return TRUE to flag the file as excluded.
 *
 * @param array $file
 *   The file array to be copied.
 */
function hook_cloudinary_copy_exclusion_criteria(array $file) {
  $exclusion_paths = [
    'sites/default/files/profiles/staff/*',
    'sites/default/files/thumbnails/*',
  ];

  $wrapper = file_stream_wrapper_get_instance_by_uri($file['uri']);
  $local_path = $wrapper->getDirectoryPath() . '/' . file_uri_target($file['uri']);

  if (drupal_match_path($local_path, explode("\n", $exclusion_paths))) {
    return TRUE;
  }

  return FALSE;
}
