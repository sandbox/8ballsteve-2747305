Cloudinary Copy Module
================================================================================

A simple module to compliment the awesome Cloudinary suite of modules
https://www.drupal.org/project/cloudinary. This module allows you to copy all your
publicly stored media assets to a pre-configured cloudinary account.

Since file naming is closely managed in Cloudinary to ensure compatabilty this module
also requires the Transliteration module (https://www.drupal.org/project/transliteration).
This will rename files on copy operations.

Additionally as your file system will likley contain some level of folder structure you
will need to ensure the "auto create folders" setting is defined on your Cloudinary
account and the following patch may be required to ensure a smooth copy operation:

https://www.drupal.org/files/issues/cloudinary-stream_wrapper-api-folder-creation-2535602-3-7.x.patch


Installation
--------------------------------------------------------------------------------

Download and enable the module.


Usage
--------------------------------------------------------------------------------

The module provides both a UI and a Drush CLI interface.

1. UI

Visit http://mysite.com/admin/config/media/cloudinary/cloud-copy to see all eligible files for
copy. Hit "copy files" to trigger a batch operation to copy files to a preconfigured Cloudinary account.

2. Drush

Run `drush cloud-copy` to trigger a batch operation on all elgible files from a terminal interface.


API
--------------------------------------------------------------------------------

The module additionall exposes a hook to allow you to exlude files based on user definable
conditions. Simply implement the hook to gain access to the file array at the point before it is
copied. Return True to stipulate that a file is excluded.
