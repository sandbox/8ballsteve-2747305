<?php

/**
 * @file
 * Utility functions for Clouidinary copy.
 */

/**
 * Retrieve managed_files that match our criteria.
 */
function cloudinary_copy_file_query() {

  $results = [];

  $query = db_select('file_managed', 'f')
    ->fields('f')
    ->condition('uri', '%' . db_like(CLOUDINARY_COPY_SOURCE_STREAM_WRAPPER) . '%', 'LIKE')
    ->execute();

  while ($record = $query->fetchObject()) {

    // Only add the record if it actually exists in the local file system.
    $wrapper = file_stream_wrapper_get_instance_by_uri($record->uri);

    // Check if the file meets our exclusion criteria.
    // @todo add default Cloudinary filesize criteria.
    if (file_exists($wrapper->realpath()) && !_cloudinary_copy_exclusion_criteria($record)) {
      $results[] = $record;
    }
  }

  return $results;
}

/**
 * Perform stream wrapper switch and filename cleanup.
 */
function cloudinary_copy_switcheroo($file) {

  if (module_exists('transliteration')) {
    $uri = str_replace($file->filename, transliteration_clean_filename($file->filename), $file->uri);
  }

  return str_replace(CLOUDINARY_COPY_SOURCE_STREAM_WRAPPER, CLOUDINARY_COPY_TARGET_STREAM_WRAPPER, $uri);
}

/**
 * Call hooks from other modules to allow this file to be excluded.
 *
 * @todo: check file size meets cloudiary criteria.
 */
function _cloudinary_copy_exclusion_criteria($file) {

  // Allow other modules to exclude this file from being copied.
  if ($results = module_invoke_all('cloudinary_copy_exclusion_criteria', (array) $file)) {
    foreach ($results as $result) {
      if ($result == TRUE) {
        return TRUE;
      }
    }
  }

  return FALSE;
}
