<?php

/**
 * @file
 * Batch processing functions.
 */

/**
 * Initialise batch function.
 */
function cloudinary_copy_process_init() {
  $results = cloudinary_copy_file_query();
  $count = count($results);

  if ($count > 0) {

    // Initialise batch.
    $batch = [
      'title' => t('Processing...'),
      'operations' => [],
      'progress_message' => t('Processed @current out of @total.'),
      'finished' => 'cloudinary_copy_batch_finished',
    ];

    // Collect records for processing.
    foreach ($results as $record) {
      $batch['operations'][] = [
        'cloudinary_copy_batch_process_file',
        [$record, $count],
      ];
    }

    // Trigger batch.
    batch_set($batch);

    // Processing via Drush?
    if (function_exists('drush_main')) {
      drush_print('Copy media - go...!!');
      $batch =& batch_get();

      // Because we are doing this on the back-end, we set progressive to false.
      $batch['progressive'] = FALSE;

      // Start processing the batch operations.
      drush_backend_batch_process();
    }
  }
  elseif (function_exists('drush_main')) {
    drush_print('No media to copy!');
  }
}

/**
 * Batch process files.
 */
function cloudinary_copy_batch_process_file($record, $count, &$context) {

  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_file'] = 0;
    $context['sandbox']['max'] = $count;
  }

  $file = file_load($record->fid);

  // Keep an un-modified record of the file (required for file_entity hooks).
  $file->original = clone $file;

  $wrapper = file_stream_wrapper_get_instance_by_uri($file->uri);

  // Skip non-writable stream wrappers.
  $writeable_stream_wrappers = file_get_stream_wrappers(STREAM_WRAPPERS_WRITE);

  // Check that we can process the file.
  if ($wrapper && isset($writeable_stream_wrappers['cloudinary'])) {

    // Sanitize file name.
    $filename = transliteration_clean_filename(basename($file->uri));
    // Build destination URI.
    $destination = cloudinary_copy_switcheroo($file);
    $realpath = $wrapper->realpath();

    // Copy and update the file record accordingly.
    if ($destination = file_unmanaged_copy($realpath, $destination)) {

      // Inform the batch engine that we are not finished,
      // and provide an estimation of the completion level we reached.
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {

        // Update our progress information.
        $context['sandbox']['progress']++;
        $context['sandbox']['current_file'] = $file->fid;
        $context['message'] = t('Now processing %file', ['%file' => $filename]);
      }

      // Update the file object.
      $file->uri = $destination;
      $file->filename = $filename;

      // Don't use file_save() as it modifies the timestamp.
      drupal_write_record('file_managed', $file, 'fid');

      // Inform modules that the file has been updated.
      module_invoke_all('file_update', $file);

      watchdog('cloudinary_copy', 'file %filename copied to %destination', ['%filename' => $filename, '%destination' => $destination], WATCHDOG_INFO);

      $context['results']['processed'][] = $filename;

    }
    else {
      // File could not be moved.
      $error = sprintf('File: %s could not be copied from %s to %s.', $file->filename, $file->uri, $destination);
    }
  }
  else {
    // File could not be read.
    $error = sprintf('File: %s could not be read at %s.', $file->filename, $file->uri);
  }

  // Any errors?
  if (isset($error)) {
    $context['results']['errors'][] = $error;
    watchdog('cloudinary_copy', $error, [], WATCHDOG_ERROR);
  }
}

/**
 * Batch finished function.
 */
function cloudinary_copy_batch_finished($success, $results, $operations) {

  if ($success) {
    // Flush page cache.
    cache_clear_all();
  }

  if (isset($results['processed'])) {
    $message  = count($results['processed']) . ' processed';
    $message .= theme_item_list([
      'items' => $results['processed'],
      'title' => t('Processed'),
      'type' => 'ul',
      'attributes' => [],
    ]);
    drupal_set_message($message);
    watchdog('cloudinary_copy', 'File processing complete.', [], WATCHDOG_ERROR);
  }

  if (isset($results['errors'])) {

    $file_errors = [
      '@errors' => format_plural(count($results['errors']), '1 file', '@count files'),
    ];

    $message = t(
      'Not all files could be copied. @errors could not be accessed and have been ignored.', $file_errors);
    $message .= theme_item_list([
      'items' => $results['errors'],
      'title' => t('Errors'),
      'type' => 'ul',
      'attributes' => [],
    ]);

    drupal_set_message($message, 'error');
    watchdog('cloudinary_copy', 'Errors occurred while processing files.', [], WATCHDOG_ERROR);
  }
}
