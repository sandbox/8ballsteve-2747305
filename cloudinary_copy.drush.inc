<?php

/**
 * @file
 * Drush integration.
 */

/**
 * Implements hook_drush_command().
 */
function cloudinary_copy_drush_command() {
  $items  = [];
  $items['cloudinary_copy'] = [
    'callback' => 'cloudinary_copy_drush_process_init',
    'description' => dt('Copy managed files to cloudinary'),
    'options' => [],
    'aliases' => ['cloud-copy'],
  ];
  return $items;
}

/**
 * Implements hook_drush_help().
 */
function cloudinary_copy_drush_help($section) {

  switch ($section) {
    case 'drush:cloudinary_copy':

      $help = "Move managed media to a pre-configured cloudinary instance.";

      break;

  }

  return dt($help);
}

/**
 * Drush callback function.
 */
function cloudinary_copy_drush_process_init() {
  cloudinary_copy_process_init();
}
