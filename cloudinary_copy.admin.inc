<?php

/**
 * @file
 * Admin functions.
 */

/**
 * Form callback function - manually trigger a media copy from the ui.
 *
 * @todo: paginate records.
 */
function cloudinary_copy_form() {

  if (!cloudinary_sdk_init()) {
    drupal_set_message(t('Please configure your Cloudinary credentials before proceeding'), 'error');
    return;
  }

  $form = [];

  $results = cloudinary_copy_file_query();

  if (count($results) == 0) {
    $form['description']['#markup'] = t('There are currently no files that need to be copied.');
    return $form;
  }

  // Generate file list.
  $rows = [];
  $header = [
    t('Original path'),
    t('Destination path'),
  ];

  foreach ($results as $row) {
    $rows[] = [$row->uri, cloudinary_copy_switcheroo($row)];
  }

  return confirm_form($form, 'Copy files to Cloudinary?', 'admin/config/media/cloudinary', theme('table', ['header' => $header, 'rows' => $rows]), t('Copy files'));
}

/**
 * Process form submission.
 */
function cloudinary_copy_form_submit($form, &$form_state) {
  cloudinary_copy_process_init();
}
